//
//  EmojiArtViewController.swift
//  emojiArt
//
//  Created by XeVoNx on 17/05/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

//THIS EXTENSION LIVES IN THE CONTROLLER
extension EmojiArt.EmojiInfo {
    //The UILabel is UI, cant be in the Model but it can be in the Controller
    
    //Failable Initializer
    init?(label: UILabel) {
        if let attributedText = label.attributedText, let font = attributedText.font {
            x = Int(label.center.x)
            y = Int(label.center.y)
            text = attributedText.string
            size = Int(font.pointSize)
        } else {
            return nil //Failing
        }
    }
}

class EmojiArtViewController: UIViewController, UIDropInteractionDelegate, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDragDelegate, UICollectionViewDropDelegate {
   
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
////        testing
//        if let url = try? FileManager.default.url(
//        for: .documentDirectory,
//        in: .userDomainMask,
//        appropriateFor: nil,
//        create: true
//        ).appendingPathComponent("Untitled.json") { //and append file URL to it
//            //UIDocuments only have this initializer
//            document = EmojiArtDocument(fileURL: url)
//        }
//        
//    }
    
    //MARK: - Model
    
    var emojiArt: EmojiArt?{
        //Computed, anytime somneone needs the model, look through  UI and make it
        get {
            if let url = emojiArtBackgroundImage.url {
                //CompatctMap is map that ignores nil values, get list of Labels and...
                let emojis = emojiArtView.subviews.compactMap{ $0 as? UILabel }.compactMap { EmojiArt.EmojiInfo(label: $0) } // ... get [info] for them
                return EmojiArt(url: url, emojis: emojis)
            }
            return nil
        }
        //And each time someone sets the Model, update the UI to reflect that (SYNC)
        set {
            emojiArtBackgroundImage = (nil, nil) //Tuple of nil
            //Send a message to each label to remove itself
            emojiArtView.subviews.compactMap { $0 as? UILabel }.forEach {$0.removeFromSuperview() }
            //Put the new url in
            if let url = newValue?.url {
                imageFetcher = ImageFetcher(fetch: url) { (url, image) in
                    //Update UI on main when URL comes back
                    DispatchQueue.main.async {
                        self.emojiArtBackgroundImage = (url, image)
                        newValue?.emojis.forEach {
                            //attributedString is a method from Utilities.swift
                            let attributedText = $0.text.attributedString(withTextStyle: .body, ofSize: CGFloat($0.size))
                            self.emojiArtView.addLabel(with: attributedText, centeredAt: CGPoint(x: $0.x, y: $0.y))
                        }
                    }
                }
            }
        }
    }
    
    //Need to be able to talk to the Document (set in the code from fileChooser)
    var document: EmojiArtDocument?
    
    
    /*  You should never have a save button in document apps
        Would be better to updateChangeCount each time a change is done
        (Automate Change Tracking)
        by using an EmojiArtViewDelegate
        !NOT! variable in view (reference to the Controller)
     */
    @IBAction func save(_ sender: UIBarButtonItem? = nil) { //optional w/default to call save()
        
        //tell the document to look at the model
        document?.emojiArt = emojiArt
        if document?.emojiArt != nil {
            //Tell the document that a change has happened
            document?.updateChangeCount(.done) // .undo .redo
            document?.thumbnail = emojiArtView.snapshot //Utilities.swift
        }
        
    }
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        
        //If i had (Automate Change Tracking), i wouldn't need to save()
        save()
        if document?.emojiArt != nil {
            document?.thumbnail = emojiArtView.snapshot //Utilities.swift
        }
        
        dismiss(animated: true) { //wait to be dismissed to close
            self.document?.close() //also has a {} completion handler, .success etc....
        }
        
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        document?.open {success in
            if success {
                //last part of url without file extension
                self.title = self.document?.localizedName
                //Get the model from document (that it got from the file)
                self.emojiArt = self.document?.emojiArt
            }
        }
        
    }
    
    
    
    //Why 2 Views: 1)Keep track at controller level what's dropped in
    //2) even if the artView is small drag always works

    //MARK: - Drop Interaction Delegate Methods

    @IBOutlet weak var dropZone: UIView! {
        didSet {
            dropZone.addInteraction(UIDropInteraction(delegate: self))
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView! {
        //Enable zooming
        didSet {
            scrollView.minimumZoomScale = 0.1
            scrollView.maximumZoomScale = 5.0
            scrollView.delegate = self
            scrollView.addSubview(emojiArtView)
        }
    }
    //MARK: - Scroll View delegate methods
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return emojiArtView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        //And if it gets too big its gonna lose since its low priority
        scrollViewHeight.constant = scrollView.contentSize.height
        scrollViewWidth.constant = scrollView.contentSize.width
    }
    
    var emojiArtView = EmojiArtView()
    
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewWidth: NSLayoutConstraint!
    
    //_BackgroundStorage in there (we never set this directly)
    private var _emojiArtBackgroundImageURL: URL?
    //But it is set there
    var emojiArtBackgroundImage: (url: URL?, image: UIImage?) {
        get {
            return (_emojiArtBackgroundImageURL, emojiArtView.backgroundImage)
        }
        set {
            _emojiArtBackgroundImageURL = newValue.url
            //always the size of frame needs to be same as content size; also give an initial zoom
            scrollView?.zoomScale = 1.0
            emojiArtView.backgroundImage = newValue.image
            let size = newValue.image?.size ?? CGSize.zero
            emojiArtView.frame = CGRect(origin: CGPoint.zero, size: size)
            scrollView?.contentSize = size
            //Opt chain in case this gets called from a prepare
            scrollViewHeight?.constant = size.height
            scrollViewWidth?.constant = size.width
            if let dropZone = self.dropZone, size.width > 0, size.height > 0 {
                scrollView?.zoomScale = max(dropZone.bounds.size.width / size.width, dropZone.bounds.size.height / size.height)
            }
        }
    }
     
    private var addingEmoji = false
    
    @IBAction func addEmoji(_ sender: UIButton) {
        addingEmoji = true
        print(1)
        //this + btn and textfield are in a different section than the rest
        emojiCollectionView.reloadSections(IndexSet(integer: 0))
    }
    
    
    //MARK: - Emoji collection methods
    @IBOutlet weak var emojiCollectionView: UICollectionView! {
        //Every time you drop colleciton in a mvc and ctrl drag (as opposed to prepackaged collection view)
        didSet {
            emojiCollectionView.dataSource = self
            emojiCollectionView.delegate = self
            emojiCollectionView.dragDelegate = self
            emojiCollectionView.dropDelegate = self
            //By default CV doesn't allow dragNdrop on iphones
            emojiCollectionView.dragInteractionEnabled = true
            
        }
    }
    
    //Account for accesibility settings 64 Points scaled by accesibility
    //Important to change collection view size as well
    private var font: UIFont {
        return UIFontMetrics(forTextStyle: .body).scaledFont(for: UIFont.preferredFont(forTextStyle: .body).withSize(64.0))
    }
    
    //Apply the closure on each char, -> emojis is [String]
    var emojis = "🇹🇩🇹🇱🇲🇾🇯🇲🇯🇪🇬🇧🇨🇨🇪🇸🇹🇷🇪🇹🇨🇳🇦🇽🇯🇵🏴󠁧󠁢󠁳󠁣󠁴󠁿🇪🇺🇻🇪🇦🇸🏴‍☠️🇶🇦🇨🇾".map { String($0) }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
        //!adjust! numberOfItemsInSection, cellForItemAt, collectionViewLayout
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if addingEmoji && indexPath.section == 0 {
            return CGSize(width: 300, height: 80)
        } else {
            return CGSize(width: 80, height: 80)
        }
    }
    
    //MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let inputCell = cell as? TextFieldCollectionViewCell {
            //show kb if i'm displaing the textField
            inputCell.textField.becomeFirstResponder()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
            case 0: return 1 //either the +btn or textField
            case 1: return emojis.count
            default: return 0
        }
        
       }
    
    //This gets called when reloading collection
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmojiCell", for: indexPath)
            if let emojiCell = cell as? EmojiCollectionViewCell {
                let text = NSAttributedString(string: emojis[indexPath.item], attributes: [.font:  font])
                emojiCell.label.attributedText = text
            }
            return cell
        } else if addingEmoji {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmojiInputCell", for: indexPath)
            //Implement the optional closure (called when TF resigns)
            if let inputCell = cell as? TextFieldCollectionViewCell {
                inputCell.resignationHandler = { [weak self] in
                    if let text = inputCell.textField.text {
                        //add an arr of those characters to the model, make sure only the unique ones
                        self?.emojis = (text.map {String($0) } + self!.emojis).uniquified
                    }
                    self?.addingEmoji = false //so updating shows the + btn agian
                    
                    //Update the view to reflect the new model
                    self?.emojiCollectionView.reloadData()
                }
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddEmojiButtonCell", for: indexPath)
            return cell
        }
        
    }
    
    //CV DragDelegate - tell sys what we're dragging (forBeginning), this CV gives us the indexPath
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        //This is smth in the dragSection that says: this is a LOCAL drag and here's the CONTEXT of it
        session.localContext = collectionView //since it's coming from the collectionView
        //this also makes it easy to .move items inside CV and .copy anywhere else
        
        return dragItems(at: indexPath)
    }
    
    //Implement multiple items drag
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        //Item providers: NSUrl, NSAttributedString, NSString, UIImage
        // ! disable dragging when editing the emoji
        if !addingEmoji, let attributedString = (emojiCollectionView.cellForItem(at: indexPath) as? EmojiCollectionViewCell)?.label.attributedText {
            let dragItem = UIDragItem(itemProvider: NSItemProvider(object: attributedString))
            //OR within this app, no need for async code to Drag
            dragItem.localObject = attributedString
            return [dragItem]
        } else { //return empty if there's nothing to drag(that fits)
            return []
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        //Dont accept drops we can't understand, accept any strings:
        return session.canLoadObjects(ofClass: NSAttributedString.self)
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        //here we return thats drop proposal kind of .copy/.drop/.cancel
        //Intent -> insertInto this cell or add a new cell insertAt?
        
        if let indexPath = destinationIndexPath, indexPath.section == 1 {
            //how to know if i'm in my own collection view to not copy the items?
            //cast because any does not implement equatable
            let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
            return UICollectionViewDropProposal(operation: isSelf ? .move : .copy, intent: .insertAtDestinationIndexPath)
        } else {
            //dont allow repositioning emojis in seciton 0
            return UICollectionViewDropProposal(operation: .cancel)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        //we need to update the model & collectionView
        //there are 2 drop cases -from collection view & -from other app
        
        //destination might be nil if you drag at the edges
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item:0, section:0)
        //UICollectionViewDropItems have some useful info
        for item in coordinator.items {
            //we could be droping multiple items, one by one
            
            //then this drag is coming from myself
            if let sourceIndexPath = item.sourceIndexPath {
                if let attributedString = item.dragItem.localObject as? NSAttributedString {
                    //Don't reload data in the middle of a drag,
                    //each step would have to have the model in sync with the CV
                    //FIX: batchUpdates -do all as one operation
                    collectionView.performBatchUpdates({
                        emojis.remove(at: sourceIndexPath.item)
                        emojis.insert(attributedString.string, at: destinationIndexPath.item)
                        
                        collectionView.deleteItems(at: [sourceIndexPath])
                        collectionView.insertItems(at: [destinationIndexPath])
                    }) //This also has a ,completion: {} if you need it
                    
                    //Animate the drag
                    coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
                }
            } else {
                //if you drag from outside the app, that info won't be instantly available -> fetch
                //and the CV/TV needs a PLACEHOLDER, you decide which type of cell is that
                let placeholderContext = coordinator.drop(
                    item.dragItem,
                    to: UICollectionViewDropPlaceholder(insertionIndexPath: destinationIndexPath,
                    reuseIdentifier: "DropPlaceholderCell")
                )
                //{} in a closure you can init this cell to set outlets, its not calling deque otherwise
                    
                //one item, unlike loadItems
                item.dragItem.itemProvider.loadObject(ofClass: NSAttributedString.self) { (provider, error) in
                    DispatchQueue.main.async {
                        if let attributedString = provider as? NSAttributedString {
                        //replace the placeholder with a cell by calling normal cellForIndexPath methods
                        placeholderContext.commitInsertion(dataSourceUpdates: {insertionIndexPath in
                            //insertionIndex might be different that the destinationIndex (new cells added/removed)
                            
                            self.emojis.insert(attributedString.string, at: insertionIndexPath.item)
                        })
                        } else {
                            //can't get attributedString from the provider, maybe there's an error
                            placeholderContext.deletePlaceholder()
                        }
                    }
                }
              
            }
        }
    }
    
    
    
    
    //Only allow drags that have img and url for that img (objc compatible)
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSURL.self) && session.canLoadObjects(ofClass: UIImage.self)
    }
    
    //Copy, this is gonna come from outside the app, never cancel
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .copy)
    }
    
    //Takes url, fetches img, make sure its img, then calls back with the handler you give to it
    //has a backup img,if i fetch url and its not of an img then use backup img(with local url in the filesystem
    var imageFetcher: ImageFetcher! //takes no args except for a closure to callback
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        
        //when it comes back its not on the main queue
        imageFetcher =  ImageFetcher() { (url, image) in
            DispatchQueue.main.async {
                self.emojiArtBackgroundImage = (url,image)
            }
        }
        
        //If let as? because type of those [] is NSProvider nsurls+images
        session.loadObjects(ofClass: NSURL.self) { nsurls in
            if let url = nsurls.first as? URL {
                self.imageFetcher.fetch(url)
            }
        
        }
        session.loadObjects(ofClass: UIImage.self) { images in
            if let image = images.first as? UIImage {
            self.imageFetcher.backup = image
            }
        }
        
    }


    
}
