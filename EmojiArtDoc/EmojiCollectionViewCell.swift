//
//  EmojiCollectionViewCell.swift
//  emojiArt
//
//  Created by XeVoNx on 27/05/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

class EmojiCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    //you can put logic for this cell here
}
