//
//  EmojiArt.swift
//  emojiArt
//
//  Created by XeVoNx on 03/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import Foundation

//Make it persistent by making this CODABLE,be able to turn itself into JSON and JSON will be the file format
struct EmojiArt: Codable {
    //The JSON keys will be the same as prop names,could be changed by adding privateEnum..keys..
    
    var url: URL
    var emojis = [EmojiInfo]()
    
    struct EmojiInfo: Codable {
        //Ints instead of CGFloats because this is a Model
        let x: Int
        let y: Int
        let text: String
        let size: Int
    }
    
    init?(json: Data) {
        if let newValue = try? JSONDecoder().decode(EmojiArt.self, from: json){
            self = newValue
        } else {
            return nil
        }
    }
    
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    init(url: URL, emojis: [EmojiInfo]) {
        self.url = url
        self.emojis = emojis
    }
}
