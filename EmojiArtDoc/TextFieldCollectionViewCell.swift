//
//  TextFieldCollectionViewCell.swift
//  EmojiArt
//
//  Created by CS193p Instructor.
//  Copyright © 2017 CS193p Instructor. All rights reserved.
//

import UIKit

class TextFieldCollectionViewCell: UICollectionViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var textField: UITextField! {
        didSet {
            textField.delegate = self
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //remove keyboard when return is pressed
        textField.resignFirstResponder()
        return true
    }
    
    //CollectiopnViewCells don't have a pointer to the Collection:
    var resignationHandler: (() -> Void)?
        //Fix: And everyone interested when textField resigns just has to set this
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //Fix: Just call that handler
        resignationHandler?()
    }
}
