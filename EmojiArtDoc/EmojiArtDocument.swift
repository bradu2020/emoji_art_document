//
//  EmojiArtDocument.swift
//  EmojiArtDoc
//
//  Created by XeVoNx on 04/06/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

class EmojiArtDocument: UIDocument {
    
    var emojiArt: EmojiArt?
    
    //Returns Any (not a Data), could be a Directory full of files (wrapper)
    override func contents(forType typeName: String) throws -> Any {
        // Encode your document with an instance of NSData or NSFileWrapper
        return emojiArt?.json ?? Data() //or empty Document
    }
    
    //This is a unique identifier (UTI) "ofType typeName: String?",
    //useful if opening multiple document types
    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        // Load your document from contents
        if let json = contents as? Data {
            emojiArt = EmojiArt(json: json)
        }
    }
    
    //Now can use the api of UIDocument - Async opening/saving,
    //working over iCloud, auto-saving etc - In EmojiArtViewController
    
    
    var thumbnail: UIImage? //will be set each time Done is pressed | "close" is called
    //Set the thumbnail a miniature of the document
    //This just returns a dict of the file attributes from its superclass
    override func fileAttributesToWrite(to url: URL, for saveOperation: UIDocument.SaveOperation) throws -> [AnyHashable : Any] {
        var attributes = try super.fileAttributesToWrite(to: url, for: saveOperation)
        if let thumbnail = self.thumbnail {
            //add a thumbnail to it (can be any size if not too small)
            attributes[URLResourceKey.thumbnailDictionaryKey] = [URLThumbnailDictionaryItem.NSThumbnail1024x1024SizeKey:thumbnail]
        }
        return attributes
    }
}

